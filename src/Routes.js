import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Contacts } from './components/Contacts/Contacts';
import { Contact } from './components/Contact/Contact';

const Stack = createStackNavigator();

export function Routes() {
  return (
    <Stack.Navigator
      initialRouteName="Contacts"
      headerMode="screen"
    >
      <Stack.Screen
        name="Contacts"
        component={Contacts}

      />
      <Stack.Screen
        name="Contact"
        component={Contact}
      />
    </Stack.Navigator>
  )
}