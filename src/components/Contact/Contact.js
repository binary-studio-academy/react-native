import React, { useEffect, useState, Fragment } from "react";
import { StyleSheet, Image, Text, View, Linking, Button } from "react-native";
import * as Contacts from "expo-contacts";

export const Contact = (props) => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      const currUser = await Contacts.getContactByIdAsync(
        props.route.params.contactId
      );
      setUser(currUser);
      setLoading(false);
    })();
  }, []);

  const handleOnCall = (number) => {
    const url = `tel:${number}`;
    Linking.openURL(url);
  }

  const handleOnDelete = (id) => {
    Contacts.removeContactAsync(id);
  }

  return (
    <Fragment>
      {loading ? (
        <Text>Loading...</Text>
      ) : (
        <View style={styles.wrapper}>
          <View style={{ marginTop: 10 }}>
            <Image
              style={styles.image}
              source={(() => {
                const uri = user.imageAvailable
                  ? user.image.uri
                  : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";
                return { uri };
              })()}
            />
            <View style={styles.contact}>
              <Text style={styles.contactData}>Name: {user.firstName || user.name}</Text>
              <Text style={styles.contactData}>Phone: {user.phoneNumbers[0].number}</Text>
            </View>
          </View>
          <View style={{ width: "100%" }}>
            <Button
              color="green"
              title="Call"
              onPress={() => handleOnCall(user.phoneNumbers[0].number)}
            />
            <Button
              color="grey"
              title="Delete"
              onPress={() => handleOnDelete(user.id)}
            />
          </View>
        </View>
      )}
    </Fragment>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: '#1e2128',
  },
  image: {
    alignSelf: 'center',
    width: 100,
    height: 100,
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 3,
    borderColor: "#fff",
  },
  contact: {
    marginTop: 15,
  },
  contactData: {
    color: '#fff',
  }
});