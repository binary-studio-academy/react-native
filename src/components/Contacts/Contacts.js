import React, { useEffect, useState } from "react";
import * as eContacts from "expo-contacts";
import {
  Text,
  View,
  FlatList,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

const UserContact = (props) => {
  const name = props.contact.name || 'No name';
  const phone = props.contact.phoneNumbers ? props.contact.phoneNumbers[0].number : 'No number';

  return (
    <TouchableOpacity onPress={() => props.navigation.navigate("Contact", { contactId: props.contact.id })}>
      <View style={contactStyles.contact}>
        <Text>{name}</Text>
        <Text>{phone}</Text>
      </View>
    </TouchableOpacity>
  )
}

export const Contacts = (props) => {
  const [search, setSearch] = useState('');
  const [userContacts, setUserContacts] = useState([]);
  const [showContacts, setShowContacts] = useState([]);

  useEffect(() => {
    (async () => {
      const { status } = await eContacts.requestPermissionsAsync();
      if (status === "granted") {
        let { data } = await eContacts.getContactsAsync({
          fields: [eContacts.Fields.PhoneNumbers],
        });

        data = data.filter((contact) => {
          if (contact.phoneNumbers) {
            return (
              contact.firstName &&
              contact.phoneNumbers.length &&
              contact.phoneNumbers[0].number
            );
          }
        });
        setUserContacts(data);
        setShowContacts(data);
      }
    })();
  }, []);

  const handleSearch = (value) => {
    if (value) {
      const updatedContacts = userContacts.filter(contact => contact.name.toLowerCase().substr(0, value.length) === value.toLowerCase());
      setShowContacts(updatedContacts);
    } else {
      setShowContacts(userContacts);
    }
    setSearch(value);
  }

  return (
    <View style={styles.wrapper}>
      <TextInput
        style={styles.search}
        placeholder="Search"
        placeholderTextColor="#fff"
        value={search}
        onChangeText={handleSearch}
      />
      <View style={styles.contactList}>
      {showContacts.length ? (showContacts.map(
        (contact) => <UserContact key={contact.id} contact={contact} navigation={props.navigation} />
        )
      ) : null}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#1e2128',
  },
  search: {
    marginTop: 10,
    paddingVertical: 5,
    paddingHorizontal: 20,
    width: '80%',
    color: '#ffffff',
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 20,
  },
  contactList: {
    marginTop: 10,
    width: '80%',
  }
});

const contactStyles = StyleSheet.create({
  contact: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    backgroundColor: 'white',
    borderRadius: 20,
  }
})